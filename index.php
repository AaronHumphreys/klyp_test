<?php

function movies($colour) {

    $query = array(
        'apikey' => '5rf7x2rcs995gze6k5agqwdx',
        'q' => $colour
    );

    $q = http_build_query($query, '', '&');

    $tomato = 'http://api.rottentomatoes.com/api/public/v1.0/movies.json?' . $q;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $tomato);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $resp = curl_exec($ch);
    curl_close($ch);

    $data = json_decode($resp);
    return $data;
}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Test for Klyp</title>
        
        <style>
            html{
                background-color: #bdc3c7;
            }
            h3{
                text-align: center;
            }
            .red{
                background-color: #e74c3c;
            }
            .green{
                background-color: #27ae60;
            }
            .blue{
                background-color: #3498db;
            }
            .yellow{
                background-color: #f1c40f;
            }
            table, tr, th, td{
                border: 1px solid black;
            }
            table{
                width: 80%;
                margin: 15px auto;
            }
            th:first-child{
                width:75%;
            }
            th:last-child{
                width: 15%;
            }
        </style>
        
    </head>
    <body>
        <div id="red">
            <h3>Movie titles that contain 'red'</h3>
            <table>
                <tr><th>Title</th><th>Year</th><th>Runtime</th></tr>
            <?php
                $red = movies('red');
                $movies = $red->movies;
                $regex = '/Red\b/';
                foreach ($movies as $movie) {
                    $runtime = $movie->runtime;
                    $title = preg_replace($regex, '<span class="red">Red</span> ', $movie->title);
                    if($runtime !== ''){
                        $runtime = $runtime.' mins';
                    }else
                        {$runtime = 'N/A';}
                    echo('<tr><td>'.$title.'</td><td>'.$movie->year.'</td><td>'.$movie->runtime.' mins</td></tr>');
                }
            ?>
            </table>
        </div>
        <br>
        
        <div id="green">
            <h3>Movie titles that contain 'green'</h3>
            <table>
                <tr><th>Title</th><th>Year</th><th>Runtime</th></tr>
            <?php
                $green = movies('green');
                $regex = '/green\b/i';
                $movies = $green->movies;
                
                foreach ($movies as $movie) {
                    $runtime = $movie->runtime;
                    $title = preg_replace($regex, '<span class="green">Green</span>', $movie->title, 1);
                    if($runtime !== ''){
                        $runtime = $runtime.' mins';
                    }else
                        {$runtime = 'N/A';}
                    echo('<tr><td>'.$title.'</td><td>'.$movie->year.'</td><td>'.$runtime.'</td></tr>');
                }
            ?>
            </table>
        </div>
        <br>
        
        <div id="blue">
            <h3>Movie titles that contain 'blue'</h3>
            <table>
                <tr><th>Title</th><th>Year</th><th>Runtime</th></tr>
            <?php
                $blue = movies('blue');
                $regex = '/blue\b/i';
                $movies = $blue->movies;
                foreach ($movies as $movie) {
                    $runtime = $movie->runtime;
                    $title = preg_replace($regex, '<span class="blue">Blue</span>', $movie->title, 1);
                    if($runtime !== ''){
                        $runtime = $runtime.' mins';
                    }else
                        {$runtime = 'N/A';}
                    echo('<tr><td>'.$title.'</td><td>'.$movie->year.'</td><td>'.$movie->runtime.' mins</td></tr>');
                }
            ?>
            </table>
        </div>
        <br>
        
        <div id="yellow">
            <h3>Movie titles that contain 'yellow'</h3>
            <table>
                <tr><th>Title</th><th>Year</th><th>Runtime</th></tr>
            <?php
                $yellow = movies('yellow');
                $regex = '/yellow\b/i';
                $movies = $yellow->movies;
                foreach ($movies as $movie) {
                    $runtime = $movie->runtime;
                    $title = preg_replace($regex, '<span class="yellow">Yellow</span>', $movie->title, 1);
                    if($runtime !== ''){
                        $runtime = $runtime.' mins';
                    }else
                        {$runtime = 'N/A';}
                    echo('<tr><td>'.$title.'</td><td>'.$movie->year.'</td><td>'.$movie->runtime.' mins</td></tr>');
                }
            ?>
            </table>
        </div>
    </body>
</html>


